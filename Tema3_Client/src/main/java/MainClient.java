import java.io.IOException;
import java.util.Arrays;

public class MainClient {
    public static void main(String[] args) throws IOException {

        Host host = new Host();
        host.connect();
        host.addNewLocation(0, "Brasov", "Aa", Arrays.asList(1,2,3,4));
        host.addNewLocation(1, "Brasov", "Bb", Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12));
        host.addNewLocation(2, "Brasov", "Cc", Arrays.asList(4,5));
        host.addNewLocation(3, "Deva", "Dd", Arrays.asList(4,5,6,7,8,11,12));
        host.addNewLocation(3, "Oradea", "Dd", Arrays.asList(4,5,6,7,8,11,12));

        Client client = new Client();
        client.connect();
        client.findByCity("Oradea");
        client.findByCity("Brasov");
        client.book(2, Arrays.asList(1,2));
        client.book(2, Arrays.asList(4,5));
        client.book(2, Arrays.asList(4,5));

        Client client2 = new Client();
        client2.connect();
        client2.book(0, Arrays.asList(1,2,3,4));

        Host host2 = new Host();
        host2.connect();
        host2.addNewLocation(4, "Sighisoara", "Ee", Arrays.asList(2,4,6,8,10,12));

        host.disconnect();
        host2.disconnect();
        client.disconnect();
        client2.disconnect();

    }
}
