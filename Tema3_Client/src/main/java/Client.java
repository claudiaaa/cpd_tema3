import java.io.*;
import java.net.Socket;
import java.util.List;

public class Client {
    Socket socket;
    ObjectInputStream in;
    ObjectOutputStream out;
    String role = "client";

    //clientul se conecteaza la server trimitand rolul pe care il are
    public void connect() throws IOException {
        try {
            socket = new Socket("127.0.0.1", 1234);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            out.writeObject(role);
            System.out.println(in.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
//se face o filtrare a locatiilor in functie de orasul ales
    public void findByCity(String city) {
        Integer action = 1;
        try{
            out.writeObject(action);
            System.out.println((String)in.readObject());
            out.writeObject(city);
            String input;
            while ((input = (String)in.readObject()) != null) {
                if (!".".equals(input)) {
                    System.out.println(input);
                }
                else {
                    break;
                }
            }
        }
        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

//se face o rezervare a locatiei in functie de un id, in lunile trimise ca lista de Integers
    public void book(Integer id, List<Integer> months) {
        Integer action = 2;
        try{
            out.writeObject(action);
            out.writeObject(id);
            out.writeObject(months);
            String input;
            while ((input = (String)in.readObject()) != null) {
                if (!".".equals(input)) {
                    System.out.println(input);
                }
                else {
                    break;
                }
            }
        }
        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() throws IOException {
        in.close();
        out.close();
        socket.close();
    }
}
