import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Host {
    Socket socket = new Socket();
    ObjectInputStream in;
    ObjectOutputStream out;
    String role = "host";

    //gazda se conecteaza la server trimitand rolul pe care il are
    public void connect(){
        try {
            socket = new Socket("127.0.0.1", 1234);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            out.writeObject(role);
            System.out.println(in.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
//adaugarea unei noi locatii: id, oras, numele locatiei si lesta lunilor in care este disponibila
    public void addNewLocation(Integer id, String city, String name, List<Integer> months){
        Integer action = 0;
        try{
            out.writeObject(action);
            out.writeObject(id);
            out.writeObject(city);
            out.writeObject(name);
            out.writeObject(months);
            String input;
            while ((input = (String)in.readObject()) != null) {
                if (!".".equals(input)) {
                    System.out.println(input);
                }
                else {
                    break;
                }
            }
        }
        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    public void disconnect() throws IOException {
        in.close();
        out.close();
        socket.close();
    }
}
