import jdk.swing.interop.SwingInterOpUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AirBNBHandler extends Thread{
  private Socket socketForClient;
  private String tvShowName;
  private ObjectInputStream fromClient;
  private ObjectOutputStream toClient;
  private ArrayList<Location> locations;
  private String[] cities = {"Brasov", "Sighisoara", "Constanta", "Oradea", "Sibiu", "Cluj-Napoca"};

  AirBNBHandler(Socket socketForClient, ArrayList<Location> locations) {
    this.socketForClient = socketForClient;
    this.locations=locations;
  }

  //daca gazda vrea sa adauge o noua locatie intr-un oras care nu este disponibil
  //acesta primeste un mesaj de eroare si lista oraselor disponibile,
  //iar daca orasul este corect ales, primeste un mesaj de succes si locatia este
  //adaugata in server
  public void addLocation(Integer id, String name, String city, List<Integer> months){
    Location location = new Location();
    if (Arrays.stream(cities).anyMatch(city::equals)){
      for (Integer month: months) {
        location.setCity(city);
        location.setId(id);
        location.setName(name);
        location.getDisponibility().set(month - 1, 1);
      }
      this.locations.add(location);
      try {
        toClient.writeObject("Successfully added: " + location);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    else{
      try {
        toClient.writeObject("City must be in: " + Arrays.asList(cities));
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    System.out.println(locations);
  }

  //filtrarea locatiilor in functie de orasul primit de la client
  //daca orasul exista, clientul primeste ca raspuns lista locatiilor din oras
  //altfel, primeste mesajul: City unavailable.
  public void filterByCity(String city){
    List<Location> list;
    if(Arrays.stream(cities).anyMatch(city::equals)){
      list = this.locations.stream().filter(location -> city.equals(location.getCity())).collect(Collectors.toList());
//      for(Location l: list){
//        System.out.println(l.getId() + ": " + l.getName() + l.getDisponibility());
//      }
      try {
        toClient.writeObject("Locations from " + city + ": " +list.toString());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    else {
      try {
        toClient.writeObject("City unavailable");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
//      System.out.println("City unavailable");
  }

  //rezervarea locatiei cu id-ul dat in lunile din lista trimisa
  //daca clientul doreste sa rezerve o locatie in anumite luni si cel putin o luna
  //este indisponibila, acesta nu poate face rezervarea si primeste un mesaj de eroare
  //altfel, rezervarea se realizeaza cu succes
  //iar perioada este marcata acum ca indisponibila - valoarea 0 in array-ul disponibility
  //si astfel se pastreaza consistenta
  public void bookLocation(Integer id, List<Integer> months){
    System.out.println(locations);
    Boolean good = true;
    Location l = Objects.requireNonNull(this.locations.stream().filter(location -> id.equals(location.getId())).findFirst().orElse(null));
    for(Integer month: months){
      if(l.getDisponibility().get(month - 1)==0){
        good = false;
        break;
      }
    }
    if(good){
      for (Integer month: months) {
        Objects.requireNonNull(this.locations.stream().filter(location -> id.equals(location.getId())).findFirst().orElse(null))
                .getDisponibility().set(month - 1, 0);
      }
//      System.out.println("Location was successfully booked");
      try {
        toClient.writeObject("Location was successfully booked");
      } catch (IOException e) {
        e.printStackTrace();
      }
      System.out.println(locations);
      System.out.println("Booking status: Success");
    }
    else{
      try {
        toClient.writeObject("Location is not available for the selected months!");
        System.out.println("Booking status: Fail");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  @Override
  public void run() {
    try {
      toClient = new ObjectOutputStream(socketForClient.getOutputStream());
      fromClient = new ObjectInputStream(socketForClient.getInputStream());
      String role;
      role = (String) fromClient.readObject();
      toClient.writeObject("Successfully connected");
      while(true) {
        if (role.equals("host")) {
//          System.out.println("New AirBNB host connected");
          Integer action = (Integer) fromClient.readObject();
          switch (action) {
            case 0:
              System.out.println("Preparing to add new location");
              Integer id;
              String name;
              String city;
              List<Integer> months;
              id = (Integer) fromClient.readObject();
              city = (String) fromClient.readObject();
              name = (String) fromClient.readObject();
              months = (List<Integer>) fromClient.readObject();
              addLocation(id, name, city, months);
              toClient.writeObject(".");
              break;
            default:
              System.out.println("Wrong action");
              toClient.writeObject("Wrong action");
              toClient.writeObject(".");

          }
        } else if (role.equals("client")) {
//          System.out.println("New AirBNB client connected");
          Integer action = (Integer) fromClient.readObject();
          switch (action) {
            case 1:
              System.out.println("Preparing to filter by city");
              toClient.writeObject("You can choose from: " + Arrays.asList(cities));
              String city = (String) fromClient.readObject();
              System.out.println("You choose: " + city);
              filterByCity(city);
              toClient.writeObject(".");
              break;
            case 2:
              System.out.println("Preparing to make a new booking");
              Integer id = (Integer) fromClient.readObject();
              List<Integer> months = (List<Integer>) fromClient.readObject();
              System.out.println("You want to book location with id " + id + " for the following months:" + months);
              bookLocation(id, months);
              toClient.writeObject(".");
              break;
            default:
              System.out.println("Wrong action");
              toClient.writeObject("Wrong action");
              toClient.writeObject(".");
          }

        }
      }
//      fromClient.close();
//      toClient.close();
//      socketForClient.close();

    } catch (IOException | ClassNotFoundException ioException) {
      System.out.println("Client has disconnected");
    }
  }
}
