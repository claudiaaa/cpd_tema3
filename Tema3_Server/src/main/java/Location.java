import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Location {
    private Integer id;
    private String name;
    private String city;
    private List<Integer> disponibility = new ArrayList<Integer>(Collections.nCopies(12, 0));

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Integer> getDisponibility() {
        return disponibility;
    }

    public void setDisponibility(ArrayList<Integer> disponibility) {
        this.disponibility = disponibility;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", disponibility=" + disponibility +
                '}';
    }
}
