import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;


//gazde: stocare locatii + luni in care sunt disponibile
//clienti: rezerva o locatie pentru una sau mai multe luni
// avem orase si locatii in orase
// clientii pot sa interogheze dupa oras si sa rezerve orice locatie disponibila

public class AirBNBServer  {
  public static String IP = "127.0.0.1";
  public static int PORT = 1234;
  public static ArrayList<Location> locations = new ArrayList<Location>();
  private ServerSocket airBNB = null;

  public void start() throws IOException {
    try {
      airBNB = new ServerSocket();
      airBNB.setReuseAddress(true);
      airBNB.bind(new InetSocketAddress(PORT));
      System.out.println("AirBNB Server is listening on port 1234.");

      while (true) {
        Socket socketForClient = airBNB.accept();
        System.out.println("New client connected.");
        new AirBNBHandler(socketForClient, locations).start();
      }
    }
    catch (IOException e) {
      System.out.println("Server exception: " + e.getMessage());
      e.printStackTrace();
    }
    finally {
      airBNB.close();
    }
  }

  public static void main(String[] args) throws IOException {
    AirBNBServer airBNBServer = new AirBNBServer();
    airBNBServer.start();
  }
}
